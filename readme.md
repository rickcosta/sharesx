# ShareSX - Social Network Plugin

o ShareSX foi desenvolvido para uma facil implementação de botões de compartilhamento nas redes sociais.

## Como funciona?

### Vincular JS;

```HTML
<script type="text/javascript" src="/js/shareSX.js"></script>
```

### Vincular CSS;
```HTML
<link href="/css/shareSX.css" type="text/css" rel="stylesheet"/>
```
### Elemento Receptor;

coloque no HTML o elemento que irá receber os links das redes.

```HTML
<div class="box">
    <div class="share-socials"></div>
</div>
```

### Chamada do Plugin;

Faça o seletor para o elemento HTML.

```JAVASCRIPT
 $('.share-socials').shareSX({
    title: 'Compartilhando',
    url: 'http://google.com',
    types: ['facebook','twitter','linkedin','google-plus','whatsapp']
})
```
### OBS:

No parâmetro URL, você pode passar dinâmicamente. EX: (window.location.href)

```JAVASCRIPT
$('.share-socials').shareSX({
    title: 'Compartilhando',
    url: window.location.href,
    types: ['facebook','twitter','linkedin','google-plus','whatsapp']
})
```
