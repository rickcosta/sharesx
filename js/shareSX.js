//Plugin de compartilhamento nas redes sociais

jQuery.fn.extend({
	shareSX : function(option) {
		//if(!option.types.length || !option.url){return false;}
		var html = '<ul class="share-sx">';
		
		option.types.forEach(function(type){
			var onclick = 'PopupCenterDual(\''+ getShareLink(type) +'\',\''+ option.title +'\',600,500)';
			var href = 'javascript:'
			if(type.toLowerCase() == "whatsapp"){
				onclick = '';
				href = ''+ getShareLink(type) +''
			}
			
			option.showLabels = option.showLabels == undefined ? true : option.showLabels;
			
			html += 
			'<li>\
				<a class="fa fa-'+type+'"  onclick="'+ onclick +'" href="'+ href +'">\
					' + (option.showLabels ? type : "") + '\
				</a>\
			</li>';
		});
		
		html += '</ul>';
		
		//apendando o html no seletor
		$(this).html(html);
		
		//montando url's das redes;
		function getShareLink(type){
			var link = "";
			
			if(type.toLowerCase() == "facebook")
				link = 'https://www.facebook.com/sharer/sharer.php?u=' + option.url;
			else if(type.toLowerCase() == "twitter")
				link = 'http://twitter.com/home?status=' + option.title + ': ' + option.url;			
			else if(type.toLowerCase() == "linkedin")
				link = 'https://www.linkedin.com/shareArticle?url=' + option.url + '&title=' + option.title;
			else if (type.toLowerCase() == "google-plus")
				link = 'https://plus.google.com/share?' + option.url	
			else if (type.toLowerCase() == "whatsapp")
				link = 'whatsapp://send?text=' + option.url;
			return link;		
		}
		
		//ocultando ao clicar fora
		$(document).mouseup(function (e){
			var container = $('.share-sx').parent(); // Give you class or ID
			
			if (!container.is(e.target) // if the target of the click is not the desired div or section
			    && container.has(e.target).length === 0) // ... nor a descendant-child of the container
			{
			    container.hide();
			}
		});
	}
});

//função para abrir a modal e passar lagura e altura para centralizar
function PopupCenterDual(url, title, w, h) {
			// Fixes dual-screen position Most browsers Firefox
	var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;
	width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
	height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;
	
	var left = ((width / 2) - (w / 2)) + dualScreenLeft;
	var top = ((height / 2) - (h / 2)) + dualScreenTop;
	var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left + 'toolbar=no, scrollbars=no, resizable=no, location=no, status=no');
	
	// Puts focus on the newWindow
	if (window.focus) {
	newWindow.focus();
	}
}

